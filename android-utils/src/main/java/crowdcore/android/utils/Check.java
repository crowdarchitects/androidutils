package crowdcore.android.utils;

import android.support.annotation.Nullable;

import java.util.HashMap;
import java.util.List;

/**
 * @class Check
 *
 * @brief Class to validate variables.
 *
 * Class to validate variables.
 */
public class Check
{
    /**
     * @brief Checks if the assigned string is not empty.
     *
     * @param _value The string to validate.
     *
     * @return Returns true if the string is not empty, false if it is empty or null.
     *
     * Checks if the assigned string is not empty.
     */
    public static boolean notEmpty(@Nullable String _value)
    {
        return !(_value == null || _value.isEmpty());
    }

    /**
     * @brief Checks if the assigned string is empty.
     *
     * @param _value The string to validate.
     *
     * @return Returns true if the string is empty or null, false if it not empty.
     *
     * Checks if the assigned string is empty.
     */
    public static boolean isEmpty(@Nullable String _value)
    {
        return (_value == null || _value.isEmpty());
    }

    /**
     * @brief Checks if the assigned strings have the same content.
     *
     * @param _expected The first string to validate.
     * @param _actual The second string to validate.
     *
     * @return Returns true if the strings are not null or empty and have the same content, otherwise
     * false.
     *
     * Checks if the assigned strings have the same content.
     */
    public static boolean equals(@Nullable String _expected, @Nullable String _actual)
    {
        return (notEmpty(_expected) && notEmpty(_actual) &&_expected.equals(_actual));
    }

    // -----------------------------------------------------------------------------

    /**
     * @brief Checks if the assigned HashMap is not empty.
     *
     * @param _value The HashMap to validate.
     *
     * @return Returns true if the HashMap is not empty, false if it is empty or null.
     *
     * Checks if the assigned HashMap is not empty.
     */
    public static boolean notEmpty(@Nullable HashMap _value)
    {
        return !(_value == null || _value.isEmpty());
    }

    /**
     * @brief Checks if the assigned HashMap is empty.
     *
     * @param _value The HashMap to validate.
     *
     * @return Returns true if the HashMap is empty or null, false if it not empty.
     *
     * Checks if the assigned HashMap is empty.
     */
    public static boolean isEmpty(@Nullable HashMap _value)
    {
        return (_value == null || _value.isEmpty());
    }

    // -----------------------------------------------------------------------------

    /**
     * @brief Checks if the assigned List is not empty.
     *
     * @param _value The List to validate.
     *
     * @return Returns true if the List is not empty, false if it is empty or null.
     *
     * Checks if the assigned List is not empty.
     */
    public static boolean notEmpty(@Nullable List _value)
    {
        return !(_value == null || _value.isEmpty());
    }

    /**
     * @brief Checks if the assigned List is empty.
     *
     * @param _value The List to validate.
     *
     * @return Returns true if the List is empty or null, false if it not empty.
     *
     * Checks if the assigned List is empty.
     */
    public static boolean isEmpty(@Nullable List _value)
    {
        return (_value == null || _value.isEmpty());
    }

    // -----------------------------------------------------------------------------

    /**
     * @brief Default constructor.
     *
     * Make the default constructor private, so instances cannot be created.
     */
    private Check()
    {}
}