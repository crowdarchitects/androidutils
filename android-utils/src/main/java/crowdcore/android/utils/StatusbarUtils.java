package crowdcore.android.utils;

import android.app.Activity;
import android.os.Build;
import android.view.Window;
import android.view.WindowManager;

/**
 * @class StatusbarHelper
 *
 * @brief Contains functions to manipulate the statusbar of the screen.
 *
 * Contains functions to manipulate the statusbar of the screen.
 */
public class StatusbarUtils
{
    /**
     * @brief Removes the status-bar of the current activity.
     *
     * @param _activity The current activity.
     *
     * @throws IllegalArgumentException if _activity is null.
     *
     * Removes the status-bar of the current activity.
     */
    public static void remove(Activity _activity)
    {
        if (_activity == null)
        {
            throw new IllegalArgumentException("activity cannot be null");
        }

        _activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    /**
     * @brief Sets the color for the status-bar.
     *
     * @param _window The current window.
     * @param _color The color as an integer to set.
     *
     * @throws IllegalArgumentException if arguments are null.
     *
     * Sets the color for the status-bar.
     */
    public static void setColor(Window _window, int _color)
    {
        if (_window == null)
        {
            throw new IllegalArgumentException("arguments cannot be null");
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            _window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            _window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            _window.setStatusBarColor(_color);
        }
    }

    // -----------------------------------------------------------------------------

    /**
     * @brief Default constructor.
     *
     * Make the default constructor private, so instances cannot be created.
     */
    private StatusbarUtils()
    {}
}