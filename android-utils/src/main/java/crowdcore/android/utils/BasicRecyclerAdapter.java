package crowdcore.android.utils;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public abstract class BasicRecyclerAdapter<M, VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH>
{
    public interface OnItemClickListener<M>
    {
        void onItemClick(M _article);
    }

    // -----------------------------------------------------------------------------

    public BasicRecyclerAdapter(Context _context, OnItemClickListener _listener)
    {
        items = new ArrayList<>();

        context = _context;

        listener = _listener;
    }

    // -----------------------------------------------------------------------------
    // override methods for RecyclerView

    @Override
    public void onBindViewHolder(final VH _holder, final int _position)
    {
        _holder.itemView.setOnClickListener((_view) ->
        {
            if (listener != null)
            {
                listener.onItemClick(items.get(_holder.getAdapterPosition()));
            }
        });

        onBindViewHolder(_holder, items.get(_position));
    }

    @Override
    public int getItemCount()
    {
        return items.size();
    }

    // -----------------------------------------------------------------------------

    public void set(@Nullable List<M> _items)
    {
        if (_items == null)
        {
            return;
        }

        items.clear();
        items.addAll(_items);

        notifyDataSetChanged();
    }

    public void add(@Nullable M _item, int _position)
    {
        if (_item == null)
        {
            return;
        }

        items.add(_position, _item);

        notifyItemInserted(_position);
    }

    public void add(@Nullable List<M> _items)
    {
        if (Check.isEmpty(_items))
        {
            return;
        }

        items.addAll(0, _items);

        notifyItemRangeChanged(0, _items.size() - 1);
    }

    public void remove(int _position)
    {
        items.remove(_position);

        notifyItemRemoved(_position);
    }

    // -----------------------------------------------------------------------------

    abstract protected void onBindViewHolder(VH _viewHolder, M _item);

    // -----------------------------------------------------------------------------

    protected List<M> items;

    protected Context context;

    private OnItemClickListener listener;
}