package crowdcore.android.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @class DateUtils
 *
 * @brief Contains utils-functions for dates.
 *
 * Contains utils-functions for dates.
 */
public class DateUtils
{
    /**
     * @brief Returns the current timestamp.
     *
     * @return Returns the timestamp as a {@code long}.
     *
     * Returns the current timestamp.
     */
    public static long getTimestamp()
    {
        return System.currentTimeMillis();
    }

    /**
     * @brief Converts a timestamp into a readable string.
     *
     * @param _timestamp The timestamp as an {@code long} in seconds.
     *
     * @return Returns the date as a {@link String}.
     *
     * Converts a timestamp into a readable string. Uses the format 'dd.MM.yyyy'.
     */
    public static String convertDate(long _timestamp)
    {
        // create a new Date and convert from seconds to milliseconds
        Date date = new Date(_timestamp * 1000L);

        // create a SimpleDateFormat with a specific date-pattern
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");

        // map the timestamp to the pattern and return it
        return simpleDateFormat.format(date);
    }

    // -----------------------------------------------------------------------------

    /**
     * @brief Default constructor.
     *
     * Make the default constructor private, so instances cannot be created.
     */
    private DateUtils()
    {}
}