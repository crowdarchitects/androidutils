package crowdcore.android.utils;

import android.app.Activity;
import android.support.annotation.Nullable;

/**
 * @class ActivityTransitionsUtils
 *
 * @brief Contains utils-functions for the transitions between two activities.
 *
 * Contains utils-functions for the transitions between two activities.
 */
public class ActivityTransitionsUtils
{
    /**
     * @brief Transition for opening the current activity to the right.
     *
     * @param _activity The current activity.
     *
     * Transition for opening the current activity to the right.
     */
    public static void enterFromRight(@Nullable Activity _activity)
    {
        if (_activity == null)
        {
            return;
        }

        _activity.overridePendingTransition(R.anim.enter_from_right, android.R.anim.fade_out);
    }

    /**
     * @brief Transition for closing the current activity to the right.
     *
     * @param _activity The current activity.
     *
     * Transition for closing the current activity to the right.
     */
    public static void exitToRight(@Nullable Activity _activity)
    {
        if (_activity == null)
        {
            return;
        }

        _activity.overridePendingTransition(android.R.anim.fade_in, R.anim.exit_to_right);
    }

    // -----------------------------------------------------------------------------

    /**
     * @brief Default constructor.
     *
     * Make the default constructor private, so instances cannot be created.
     */
    private ActivityTransitionsUtils()
    {}
}