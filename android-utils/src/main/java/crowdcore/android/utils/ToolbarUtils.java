package crowdcore.android.utils;

import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

/**
 * @class ToolbarUtils
 *
 * @brief Contains utils-functions for the toolbar.
 *
 * Contains utils-functions for the toolbar.
 */
public class ToolbarUtils
{
    /**
     * @brief Removes the toolbar for the current activity.
     *
     * @param _activity The current activity.
     *
     * Removes the toolbar for the current activity.
     */
    public static void remove(@Nullable AppCompatActivity _activity)
    {
        if (_activity == null || _activity.getSupportActionBar() == null)
        {
            return;
        }

        _activity.getSupportActionBar().hide();
    }

    /**
     * @brief Sets the title in the toolbar.
     *
     * @param _activity The current activity.
     * @param _title The title as a {@link String}.
     *
     * Sets the title in the toolbar.
     */
    public static void setTitle(@Nullable AppCompatActivity _activity, @Nullable String _title)
    {
        if (_activity == null || _activity.getSupportActionBar() == null || _title == null || _title.isEmpty())
        {
            return;
        }

        _activity.getSupportActionBar().setTitle(_title);
    }

    // -----------------------------------------------------------------------------

    /**
     * @brief Default constructor.
     *
     * Make the default constructor private, so instances cannot be created.
     */
    private ToolbarUtils()
    {}
}