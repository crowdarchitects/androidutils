package crowdcore.android.utils;

import android.content.Context;
import android.graphics.Point;
import android.support.annotation.Nullable;
import android.view.Display;
import android.view.WindowManager;

/**
 * @class ScreenUtils
 *
 * @brief Contains utils-functions for the current device-screen.
 *
 * Contains utils-functions for the current device-screen.
 */
public class ScreenUtils
{
    /**
     * @brief Calculates the screen-width in pixels.
     *
     * @param _context The current context.
     *
     * @return Returns the width as int. If the assigned context is null, -1 will be returned.
     *
     * Calculates the screen-width in pixels
     */
    public static Point getScreenCoordinates(@Nullable Context _context)
    {
        if (_context == null)
        {
            return null;
        }

        Display display = ((WindowManager) _context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        return size;
    }

    // -----------------------------------------------------------------------------

    /**
     * @brief Default constructor.
     *
     * Make the default constructor private, so instances cannot be created.
     */
    private ScreenUtils()
    {}
}