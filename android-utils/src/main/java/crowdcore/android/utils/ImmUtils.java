package crowdcore.android.utils;

import android.content.Context;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

/**
 * @class ImmUtils
 *
 * @brief Contains functions for input-methods.
 *
 * Contains functions for input-methods.
 */
public class ImmUtils
{
    /**
     * @brief Hides the keyboard.
     *
     * @param _view The main view with the keyboard.
     *
     * Hides the keyboard.
     */
    public static void hide(@Nullable View _view)
    {
        if (_view == null)
        {
            return;
        }

        InputMethodManager imm = (InputMethodManager) _view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);

        if (imm != null)
        {
            imm.hideSoftInputFromWindow(_view.getWindowToken(), 0);
        }
    }
}