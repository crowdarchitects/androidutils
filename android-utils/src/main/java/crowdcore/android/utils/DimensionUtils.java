package crowdcore.android.utils;

import android.content.res.Resources;

/**
 * @class DimensionUtils
 *
 * @brief Contains functions for the dimension of the device.
 *
 * Contains functions for the dimension of the device.
 */
public class DimensionUtils
{
    /**
     * @brief Converts pixel to density-pixel.
     *
     * @param _pixel The pixel as {@code float}.
     *
     * @return Returns the density-pixel as {@code float}.
     *
     * Converts pixel to density-pixel.
     */
    public static float pxToDp(float _pixel)
    {
        return (_pixel / Resources.getSystem().getDisplayMetrics().density);
    }

    /**
     * @brief Converts density-pixel to pixel.
     *
     * @param _dp The density-pixel as {@code float}.
     *
     * @return Returns the pixel as {@code float}.
     *
     * Converts density-pixel to pixel.
     */
    public static float dpToPx(float _dp)
    {
        return (_dp * Resources.getSystem().getDisplayMetrics().density);
    }

    /**
     * @brief Default constructor.
     *
     * Make the default constructor private, so instances cannot be created.
     */
    private DimensionUtils()
    {}
}