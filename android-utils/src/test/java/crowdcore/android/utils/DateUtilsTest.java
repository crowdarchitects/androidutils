package crowdcore.android.utils;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

public class DateUtilsTest
{
    // testing getTimestamp

    @Test
    public void itShouldReturnAValidTimeStamp()
    {
        long timestamp = DateUtils.getTimestamp();

        assertTrue((timestamp > 1500000000));
    }

    // -----------------------------------------------------------------------------
    // testing convertDate

    @Test
    public void itShouldConvertDateWitNegativeTimestamp()
    {
        String date = DateUtils.convertDate(-1);

        assertEquals("01.01.1970", date);
    }

    @Test
    public void itShouldConvertDateWithTimestampZero()
    {
        String date = DateUtils.convertDate(0);

        assertEquals("01.01.1970", date);
    }

    @Test
    public void itShouldConvertTimestamp555555()
    {
        String date = DateUtils.convertDate(555555);

        assertEquals("07.01.1970", date);
    }

    @Test
    public void itShouldConvertTimestamp186224578()
    {
        String date = DateUtils.convertDate(186224578);

        assertEquals("26.11.1975", date);
    }

    @Test
    public void itShouldConvertTimestamp584879400()
    {
        String date = DateUtils.convertDate(584879400);

        assertEquals("14.07.1988", date);
    }

    @Test
    public void itShouldConvertTimestamp984879400()
    {
        String date = DateUtils.convertDate(984879400);

        assertEquals("18.03.2001", date);
    }

    @Test
    public void itShouldConvertTimestamp1932423423()
    {
        String date = DateUtils.convertDate(1932423423);

        assertEquals("28.03.2031", date);
    }
}