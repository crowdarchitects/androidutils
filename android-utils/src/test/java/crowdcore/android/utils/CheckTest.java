package crowdcore.android.utils;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.assertFalse;

public class CheckTest
{
    // notEmpty - String

    @Test
    public void testStringNotEmptyWithNull()
    {
        String expected = null;

        boolean result = Check.notEmpty(expected);

        assertFalse(result);
    }

    @Test
    public void testStringNotEmptyWithEmptyString()
    {
        String expected = "";

        boolean result = Check.notEmpty(expected);

        assertFalse(result);
    }

    @Test
    public void testStringNotEmptyWithValidString()
    {
        String expected = "Mustermann";

        boolean result = Check.notEmpty(expected);

        Assert.assertTrue(result);
    }

    // -----------------------------------------------------------------------------
    // isEmpty - String

    @Test
    public void testStringIsEmptyWithNull()
    {
        String expected = null;

        boolean result = Check.isEmpty(expected);

        Assert.assertTrue(result);
    }

    @Test
    public void testStringIsEmptyWithEmptyString()
    {
        String expected = "";

        boolean result = Check.isEmpty(expected);

        Assert.assertTrue(result);
    }

    @Test
    public void testStringIsEmptyWithValidString()
    {
        String expected = "Mustermann";

        boolean result = Check.isEmpty(expected);

        assertFalse(result);
    }

    // -----------------------------------------------------------------------------
    // equals

    @Test
    public void testEqualsWithExpectedAndActualNull()
    {
        boolean result = Check.equals(null, null);

        assertFalse(result);
    }

    @Test
    public void testEqualsWithExpectedEmptyAndActualNull()
    {
        boolean result = Check.equals("", null);

        assertFalse(result);
    }

    @Test
    public void testEqualsWithValidExpectedAndActualNull()
    {
        boolean result = Check.equals("this is a test", null);

        assertFalse(result);
    }

    @Test
    public void testEqualsWithExpectedNullAndActualEmpty()
    {
        boolean result = Check.equals(null, "");

        assertFalse(result);
    }

    @Test
    public void testEqualsWithExpectedEmptyAndActualEmpty()
    {
        boolean result = Check.equals("", "");

        assertFalse(result);
    }

    @Test
    public void testEqualsWithValidExpectedAndActualEmpty()
    {
        boolean result = Check.equals("this is a test", "");

        assertFalse(result);
    }

    @Test
    public void testEqualsWithExpectedNullAndValidActual()
    {
        boolean result = Check.equals(null, "another test");

        assertFalse(result);
    }

    @Test
    public void testEqualsWithExpectedEmptyAndValidActual()
    {
        boolean result = Check.equals("", "another test");

        assertFalse(result);
    }

    @Test
    public void testEqualsWithValidExpectedAndValidActual()
    {
        boolean result = Check.equals("this is a test", "another test");

        assertFalse(result);
    }

    @Test
    public void testEqualsWithSameStrings()
    {
        boolean result = Check.equals("we are the same", "we are the same");

        Assert.assertTrue(result);
    }

    @Test
    public void testEqualsWithSameWords()
    {
        boolean result = Check.equals("navigation", "navigation");

        Assert.assertTrue(result);
    }

    @Test
    public void testEqualsWithDifferentStrings()
    {
        boolean result = Check.equals("348563894529", "Lorem ipsum");

        assertFalse(result);
    }

    // -----------------------------------------------------------------------------
    // notEmpty - HashMap

    @Test
    public void testHashMapNotEmptyWithNull()
    {
        HashMap<String, String> expectedString = null;
        HashMap<String, Integer> expectedInteger = null;
        HashMap<String, Object> expectedObject = null;

        boolean resultString = Check.notEmpty(expectedString);
        boolean resultInteger = Check.notEmpty(expectedInteger);
        boolean resultObject = Check.notEmpty(expectedObject);

        assertFalse(resultString);
        assertFalse(resultInteger);
        assertFalse(resultObject);
    }

    @Test
    public void testHashMapNotEmptyWithEmpty()
    {
        HashMap<String, String> expectedString = new HashMap<>();
        HashMap<String, Integer> expectedInteger = new HashMap<>();
        HashMap<String, Object> expectedObject = new HashMap<>();

        boolean resultString = Check.notEmpty(expectedString);
        boolean resultInteger = Check.notEmpty(expectedInteger);
        boolean resultObject = Check.notEmpty(expectedObject);

        assertFalse(resultString);
        assertFalse(resultInteger);
        assertFalse(resultObject);
    }

    @Test
    public void testHashMapNotEmptyWithValid()
    {
        HashMap<String, String> expectedString = new HashMap<>();
        expectedString.put("test", "test");
        expectedString.put("test", "test");

        HashMap<String, Integer> expectedInteger = new HashMap<>();
        expectedInteger.put("test", 4);
        expectedInteger.put("test", 34);

        boolean resultString = Check.notEmpty(expectedString);
        boolean resultInteger = Check.notEmpty(expectedInteger);

        Assert.assertTrue(resultString);
        Assert.assertTrue(resultInteger);
    }

    // -----------------------------------------------------------------------------
    // isEmpty - HashMap

    @Test
    public void testHashMapIsEmptyWithNull()
    {
        HashMap<String, String> expectedString = null;
        HashMap<String, Integer> expectedInteger = null;
        HashMap<String, Object> expectedObject = null;

        boolean resultString = Check.isEmpty(expectedString);
        boolean resultInteger = Check.isEmpty(expectedInteger);
        boolean resultObject = Check.isEmpty(expectedObject);

        Assert.assertTrue(resultString);
        Assert.assertTrue(resultInteger);
        Assert.assertTrue(resultObject);
    }

    @Test
    public void testHashMapIsEmptyWithEmpty()
    {
        HashMap<String, String> expectedString = new HashMap<>();
        HashMap<String, Integer> expectedInteger = new HashMap<>();
        HashMap<String, Object> expectedObject = new HashMap<>();

        boolean resultString = Check.isEmpty(expectedString);
        boolean resultInteger = Check.isEmpty(expectedInteger);
        boolean resultObject = Check.isEmpty(expectedObject);

        Assert.assertTrue(resultString);
        Assert.assertTrue(resultInteger);
        Assert.assertTrue(resultObject);
    }

    @Test
    public void testHashMapIsEmptyWithValid()
    {
        HashMap<String, String> expectedString = new HashMap<>();
        expectedString.put("test", "test");
        expectedString.put("test", "test");

        HashMap<String, Integer> expectedInteger = new HashMap<>();
        expectedInteger.put("test", 4);
        expectedInteger.put("test", 34);

        boolean resultString = Check.isEmpty(expectedString);
        boolean resultInteger = Check.isEmpty(expectedInteger);

        assertFalse(resultString);
        assertFalse(resultInteger);
    }

    // -----------------------------------------------------------------------------
    // notEmpty - List

    @Test
    public void testListNotEmptyWithNull()
    {
        List<String> expectedString = null;
        List<Integer> expectedInteger = null;
        List<Long> expectedLong = null;

        boolean resultString = Check.notEmpty(expectedString);
        boolean resultInteger = Check.notEmpty(expectedInteger);
        boolean resultLong = Check.notEmpty(expectedLong);

        assertFalse(resultString);
        assertFalse(resultInteger);
        assertFalse(resultLong);
    }

    @Test
    public void testListNotEmptyWithEmpty()
    {
        List<String> expectedString = new ArrayList<>();
        List<Integer> expectedInteger = new ArrayList<>();
        List<Long> expectedLong = new ArrayList<>();

        boolean resultString = Check.notEmpty(expectedString);
        boolean resultInteger = Check.notEmpty(expectedInteger);
        boolean resultLong = Check.notEmpty(expectedLong);

        assertFalse(resultString);
        assertFalse(resultInteger);
        assertFalse(resultLong);
    }

    @Test
    public void testListNotEmptyWithValid()
    {
        List<String> expectedString = new ArrayList<>();
        expectedString.add("test");
        expectedString.add("test");

        List<Integer> expectedInteger = new ArrayList<>();
        expectedInteger.add(67);
        expectedInteger.add(2385);

        List<Long> expectedLong = new ArrayList<>();
        expectedLong.add((long) 45.78);
        expectedLong.add((long) 3453.33);

        boolean resultString = Check.notEmpty(expectedString);
        boolean resultInteger = Check.notEmpty(expectedInteger);
        boolean resultLong = Check.notEmpty(expectedLong);

        Assert.assertTrue(resultString);
        Assert.assertTrue(resultInteger);
        Assert.assertTrue(resultLong);
    }

    // -----------------------------------------------------------------------------
    // isEmpty - List

    @Test
    public void testListIsEmptyWithNull()
    {
        List<String> expectedString = null;
        List<Integer> expectedInteger = null;
        List<Long> expectedLong = null;

        boolean resultString = Check.isEmpty(expectedString);
        boolean resultInteger = Check.isEmpty(expectedInteger);
        boolean resultLong = Check.isEmpty(expectedLong);

        Assert.assertTrue(resultString);
        Assert.assertTrue(resultInteger);
        Assert.assertTrue(resultLong);
    }

    @Test
    public void testListIsEmptyWithEmpty()
    {
        List<String> expectedString = new ArrayList<>();
        List<Integer> expectedInteger = new ArrayList<>();
        List<Long> expectedLong = new ArrayList<>();

        boolean resultString = Check.isEmpty(expectedString);
        boolean resultInteger = Check.isEmpty(expectedInteger);
        boolean resultLong = Check.isEmpty(expectedLong);

        Assert.assertTrue(resultString);
        Assert.assertTrue(resultInteger);
        Assert.assertTrue(resultLong);
    }

    @Test
    public void testListIsEmptyWithValid()
    {
        List<String> expectedString = new ArrayList<>();
        expectedString.add("test");
        expectedString.add("test");

        List<Integer> expectedInteger = new ArrayList<>();
        expectedInteger.add(67);
        expectedInteger.add(2385);

        List<Long> expectedLong = new ArrayList<>();
        expectedLong.add((long) 45.78);
        expectedLong.add((long) 3453.33);

        boolean resultString = Check.isEmpty(expectedString);
        boolean resultInteger = Check.isEmpty(expectedInteger);
        boolean resultLong = Check.isEmpty(expectedLong);

        assertFalse(resultString);
        assertFalse(resultInteger);
        assertFalse(resultLong);
    }
}