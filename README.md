# About #

This project represents util-functions for Android-Applications. It contains the *android-utils*-module.

## Use from another project ##

If you want to use the *android-utils*-module in your application, do the following steps.

### 1. Clone Repository ###

Clone or download this repository and place in the same directory-level like your project.

### 2. Adjust Gradle ###

Add the following code in the *settings.gradle*:
```groovy
include 'android-utils'

project(':android-utils').projectDir = new File(settingsDir, '../androidutils/android-utils')
```

Now you can use the module in the actual *build.gradle*:
```groovy
implementation project(':android-utils')
```

### 3. Define global variables ###

The module needs some global variables defined in gradle. Define the following variables in your *build.gradle* (project-level):
```groovy
ext.versions = [
        compileSdkVersion: XX,
        buildToolsVersion: 'XX.X.XX',
        minSdkVersion    : XX,
        targetSdkVersion : XX
]

ext.testDeps = [
        junit: 'junit:junit:X.XX'
]

ext.supportDeps = [
        support         : 'com.android.support:design:XX.X.XX',
        appcompat       : 'com.android.support:appcompat-v7:XX.X.XX'
]
```

Use the desired version. Also use the defined variable in your own modules. This ensures, that all modules have the same versions and preconditions.

## Tests-Execution ##
### Java-Tests ###
```sh
./gradlew test --offline
```

### Android-Tests ###
```sh
./gradlew connectedAndroidTest --offline
```